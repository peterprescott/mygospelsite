---
layout: layout.liquid
pageTitle: 'Salvation Prayer'

---
If you want to respond to [the gospel](/), try praying  
a simple 'Thank-you/Sorry/Please' prayer like this:  <br><br>

<strong>Father God, Thank-you that Jesus came and died for my sin.  <br>
Thank-you that your love is stronger than death.  <br>
Thank-you that Jesus rose from the dead.</strong><br><br>

<strong>I confess that I have sinned. I’ve done things I shouldn’t have done.  <br>
I repent--I turn away from sin. And I turn to Jesus as my Lord and Saviour.</strong><br><br>

<strong>I ask that you would fill me with your Holy Spirit.  <br>
Please fill me with your love, your joy, your peace.  <br>
In Jesus' name, Amen.</strong>  <br>  <br>

No-one can sincerely say 'Jesus is Lord' except by the Holy Spirit (1 Cor.12:3).  <br>
So if you've prayed this prayer (or something like it),  <br>
then you've been spiritually born again into God's Covenant Family!  <br><br>

But the plan of God is not to save souls simply to whisk them away <br>
to an ethereal heavenly nether-world,<br> 
but rather to revive and reform the whole of creation--<br>
which is why as well as believing in Jesus<br>
we are commanded to be baptized into the visible church.<br><br>

If you've never been baptized before then we would love to baptize you,  <br>
as a physical demonstration that you are beginning a new life as a disciple of Jesus.  <br>
And we would love to walk with you in this journey <br>
of discovering how to fulfil God's purposes in our generation.
