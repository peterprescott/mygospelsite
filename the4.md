---
layout: layout.liquid
pageTitle: 'One Gospel, four Gospels'

---

There's only [one gospel](../thegospel)--but in Scripture there are four 'Gospels'.  
And as you encounter the transforming truth in Scripture  
of God's revelation of Himself through Christ,  
you'll find that 'the gospel' changes from abstract dogma  
to deep personal conviction.

If all that was necessary was historical information  
about the ministry of Jesus, then it might have been better  
just to have one authoritative version of Jesus’ life.  
But the important thing is not just to know about Jesus  
but to personally encounter him, to know and love him.  
The four gospels were written for four different contexts,  
and so emphasise slightly different aspects of who Jesus was.  
Sometimes they even tell a particular story  
out of strict chronological order in order to do this.  

So if you’ve never read the Bible before,  
you should start with a Gospel,  
and you should ask--*Which one's **my gospel**?*

[**Matthew** is for ‘thinkers’.](../matthew)  
[**Mark** is for ‘doers’](../mark)  
[**Luke** is for ‘feelers’.](../luke)  
[ **John** is for ‘dreamers’.](../john)  
