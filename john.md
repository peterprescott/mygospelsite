---
layout: layout.liquid
pageTitle: "John's Gospel"

---

#### John is for ‘dreamers’. 
John was the last to finish writing his gospel.  
He always refers to himself as ‘the disciple Jesus loved’.  
John isn’t just interested in the miraculous wonders  
that Jesus did, but wants to know what these signs mean.  
Nor is he interested so much in Jesus’ teaching to the crowds—  
he prefers to tell us the details  
of Jesus’ conversations with individuals,  
thus inviting each individual reader to personally  
give up their small dreams for a vision of Jesus.

### [Discover John's Gospel](https://www.biblegateway.com/passage/?search=john+1-21&version=ESV).
(These [Discovery Questions](../discovery) might help.)
