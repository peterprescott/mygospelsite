---
layout: layout.liquid
pageTitle: 'One Gospel, four Gospels'

---

# The Vision?

### That you would take hold of Jesus, and His life, death and resurrection--  
### not as religious dogma, lifestyle preference, or historical nicety--   
### but as life-transformingly good news. For you. Personally.

## As [Gospel](thegospel).
