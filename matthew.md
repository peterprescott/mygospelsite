---
layout: layout.liquid
pageTitle: "Matthew's Gospel"

---

#### Matthew is for ‘thinkers’. 
Matthew was a tax accountant, the sort of person  
who could be relied upon to make sure that every penny  
was tallied up and all the financial records were in order.  
Thus it’s no surprise to find that Matthew  
has the most extensive record of Jesus’ teaching,  
and is also the most fastidious in noting  
the various ways that Jesus fulfilled Scriptural prophecy.

### [Discover Matthew's Gospel](https://www.biblegateway.com/passage/?search=matthew+1-28&version=ESV).
(These [Discovery Questions](../discovery) might help.)
