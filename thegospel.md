---
layout: layout.liquid
pageTitle: 'The Gospel'

---
<p>We are unashamed of the gospel, for it is the power of God unto salvation.<br>
It is the answer to the question of the meaning of life,<br>
which has been sought out by kings and prophets through history,<br>
and is now being revealed through the church to all people and nations.<br><br>
<em>We find this simple six-colour summary helpful <a href="https://covenantfamily.org.uk/Pages/Faith/Gospel.pdf">(click here for a longer explanation)</a>:</em>
<table><tr><td><font style="font-weight:bold; color:green;">Green</font>.</td><td> We were each <font style="font-weight:bold; color:green;">Created</font> for a unique Purpose.<br></td></tr>
<tr><td><font style="font-weight:bold; color:black;">Black</font>.</td><td> We have all been <font style="font-weight:bold; color:black;">Condemned</font> by our own Sin.<br></td></tr>
<tr><td><font style="font-weight:bold; color:red;">Red</font>.</td><td> Redemption comes only through the <font style="font-weight:bold; color:red;">Death of Jesus</font>.<br></td></tr>
<tr><td><font style="font-weight:bold; color:grey;">White</font>.</td><td> The only way to be made <font style="font-weight:bold; color:grey;">Righteous</font> is by Faith.<br></td></tr>
<tr><td><font style="font-weight:bold; color:gold;">Yellow</font>.</td><td> This alone gives Assurance of <font style="font-weight:bold; color:gold;">Eternal Life</font>.<br></td></tr>
<tr><td><font style="font-weight:bold; color:blue;">Blue</font>.</td><td> Better yet, right now we can be filled with the <font style="font-weight:bold; color:blue;">Holy Spirit</font>.<br></table></p>

Is there any reason why you wouldn’t want to receive this gift?  <br>
If so, we’d love to talk through whatever questions you might have.  <br>
But if you are ready to receive this, then you can ask God to receive this gift right now.  <br>
(If you want some help, [here's a suggestion of how to pray](../salvationprayer).)

### [Go Deeper](../the4).
