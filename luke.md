---
layout: layout.liquid
pageTitle: "Luke's Gospel"

---

#### Luke is for ‘feelers’. 
Luke was a doctor (Col. 4:18), and one of the first  
significant followers of Jesus who wasn’t Jewish.  
Being a doctor, his Gospel naturally reflects  
a heart of compassion for the sick and— 
being a Gentile—for the stranger;  
for the unwell and the unwelcome.  
But Luke isn’t interested in superficial nice feelings,  
but rather in the supernatural power of the Holy Spirit  
working through Jesus and his followers  
to bring enduring transformation.

### [Discover Luke's Gospel](https://www.biblegateway.com/passage/?search=luke+1-24&version=ESV).
(These [Discovery Questions](../discovery) might help.)
