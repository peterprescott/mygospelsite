---
layout: layout.liquid
pageTitle: "Mark's Gospel"

---

#### Mark is for doers.
Mark is the shortest Gospel, and in it everything seems  
to happen “immediately…immediately… immediately…” (1:18,20,21,23).  
Mark, the first Gospel to be written, is based on  
the eyewitness experience of the apostle Peter,  
a family friend of Mark (Acts 12:12).  
Both Mark and Peter were all-or-nothing men  
who wanted to follow Jesus completely—  
but had messed up in significant ways:  
Peter had denied Jesus after promising he would die for him;  
Mark had gotten homesick and given up  
halfway through his first missionary trip (Acts 15:38).  
Mark’s Gospel is an action-packed account  
of the ‘Suffering Servant’ who “came not to call the righteous—but sinners”.

### [Discover Mark's Gospel](https://www.biblegateway.com/passage/?search=mark+1-16&version=ESV).
(These [Discovery Questions](../discovery) might help.)
